window.onload = function() {

}

//	Функция определяющая победителя между 2-х символов, например если передать 
//	в качестве аргумента камень и ножницы на выходе получим победителя, т.е. камень
//	choice1, choice2 могут принимать значения rock, paper, scissors

function fight(choice1, choice2) {

}

//	Функция возвращает символ выбранный компьютером, с помощью функции генерации случайных чисел
//	Результатом могут быть строки rock, paper, scissors
	
function getComputerChoice() {

}

//	Добавляет css класс active элементу, которого выбрал компьютер

function computerShowActive(id) {

}

//	Сбрасывает значения классов у элементов игровой области

function resetGame() {

}

//	Выводит сообщения для игрока
//	text - Текст сообщения, type - может быть info, win, loose. Значение type влияет на цвет сообщения.

function showMessage(text, type) {

}

//	Обработчик нажатия на символ, исполняет всю логику игры

function startGame(){

}